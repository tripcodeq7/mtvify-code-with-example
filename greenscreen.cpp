#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cmath>
#include "functions.cpp"
using namespace std;

int main(int argc, char *argv[]){
	//Does minimal sanity check on command usage
        if (argc != 2) {
                  cerr << "Usage: greenscreen framecount" << endl;
                  return 1;
        }

	imagePPM image;
	//Main loop for editing images
	for(int i=1;i<stoi(argv[1]);i++){
		string path = "./main_unprocessed/"+to_string(i)+".ppm";//loads the images
		image.readFile(path);
		removegreen(image);//Runs the function remove green
		imgBlackAndWhite(image);//Makes the image black and white to hide hidden leftover green
		image.fileWrite("./main/"+to_string(i)+".ppm"); //Writes the file to the new folder
	}
	
	return 0;
}
