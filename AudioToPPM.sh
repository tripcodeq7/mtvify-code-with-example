#!/bin/bash
#First argument is the name of the audio file to create the ppm image
#Second argument is the width this will be the number of frames*framerate
#needs sox gnuplot and imagemagick
#Sanity check
if [ -z "$1" ]
then
	echo "The audiofile name is empty"
	exit
fi
if [ -z "$2" ]
then
	echo "The frames are empty (FrameRate*Number of Seconds)"
	exit
fi

sox $1 audio.dat
tail -n+3 audio.dat > audio_only.dat 

#Make audio.gpi file

echo set term png size $2,255 > audio.gpi
echo set output \"audio.png\" >> audio.gpi
echo set yr [-1:1] >> audio.gpi
echo unset key >> audio.gpi
echo unset tics >> audio.gpi
echo unset border >> audio.gpi
echo set lmargin 0 >> audio.gpi 
echo set rmargin 0 >> audio.gpi
echo set tmargin 0 >> audio.gpi
echo set bmargin 0 >> audio.gpi
echo set obj 1 rectangle behind from screen 0,0 to screen 1,1 >> audio.gpi
echo set obj 1 fillstyle solid 1.0 fillcolor rgbcolor \"#000000\" >> audio.gpi
echo plot \"audio_only.dat\" with lines lt rgb \'white\' >> audio.gpi

gnuplot audio.gpi
convert audio.png -compress none audio.ppm

#https://stackoverflow.com/questions/4468157/how-can-i-create-a-waveform-image-of-an-mp3-in-linux
#This was seriously helpful. This script mainly isn't my code
