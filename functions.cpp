#include"functions.h"
using namespace std;

/*imagePPM contains information about images and has methods
for loading and writing PPM with no compression. maxR maxG minB... contain the minumum
maximum and average colors for the entire photo sizeY and sizY contain the 
images size*/

void colorShift(imagePPM& image){
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX;j++){
			int temp = image.red.at(i).at(j);
			image.red.at(i).at(j)=image.blue.at(i).at(j);
			image.blue.at(i).at(j)=image.green.at(i).at(j);
			image.green.at(i).at(j)=temp;
		}
}

void imagePPM::blankCanvas(int y, int x){
	/*This loop rights the files to the array as well as run calculations
	For the maximum minimum and average values for each color*/
	//clears vectors
	red.clear();
	green.clear();
	blue.clear();
	sizeY=y;
	sizeX=x;
	avgR=0;
	avgG=0;
	avgB=0;
	maxR=0;
	maxG=0;
	maxB=0;
	minR=0;
	minG=0;
	minB=0;
	for(int i=0;i < sizeY; i++){
		red.push_back(vector<int>());
		green.push_back(vector<int>());
		blue.push_back(vector<int>());
		for(int j=0;j<sizeX;j++){
			red.at(i).push_back(0);
			blue.at(i).push_back(0);
			green.at(i).push_back(0);

		}
	}
}

void imagePPM::checkers(int y, int x){
	int checkerX=20;
	int checkerY=20;
	sizeX=x;
	sizeY=y;
	for(int i=0;i<sizeY;i++){
        	red.push_back(vector<int>());
		green.push_back(vector<int>());
		blue.push_back(vector<int>());
		for(int j=0;j<sizeX;j++){
			red.at(i).push_back(0);
			blue.at(i).push_back(0);
			green.at(i).push_back(0);
		}
	}
	int white=0;
	for(int i=0;i<sizeY;i+=checkerY){
		for(int j=0;j<sizeX;j+=checkerX){
			if(((j/20)+white)%2==1){
				for(int y=0;y<checkerY;y++)
					for(int x=0;x<checkerX;x++){
		                                red.at(i+y).at(j+x)=50;
                			        blue.at(i+y).at(j+x)=rand()%75+180;
                			        green.at(i+y).at(j+x)=50;
					}
			}
		}
		white+=1;
	}
}



/*This function reads the file and outputs a vector storing
Red: max min and avg Blue: max min and avg Green: max min avg*/
void imagePPM::readFile(string fileName){
	cout << fileName;
	ifstream iStream(fileName);
	//Check if the file is opened
	if(!iStream.is_open()){
		cout << "Error: File Does not exist\n";
	}
	string skip;
	char s;
	int r,g,b;
	iStream >> skip >> sizeX >> sizeY >> r;
	//This portion is for skipping the header of the ppm file
	cout << skip << " "<< sizeX << " " << sizeY;
	/*This loop rights the files to the array as well as run calculations
	For the maximum minimum and average values for each color*/
	//clears vectors
	red.clear();
	green.clear();
	blue.clear();
	avgR=0;
	avgG=0;
	avgB=0;

	for(int i=0;i < sizeY; i++){
		red.push_back(vector<int>());
		green.push_back(vector<int>());
		blue.push_back(vector<int>());
		for(int j=0;j<sizeX;j++){
			if(iStream >> r >> g >> b){
			red.at(i).push_back(r);
			blue.at(i).push_back(b);
			green.at(i).push_back(g);
			if(r>maxR)
				maxR=r;
			if(g>maxG)
				maxG=g;
			if(b>maxB)
				maxB=b;
			if(r<minR)
				minR=r;
			if(g<minG)
				minG=g;
			if(b<minB)
				minB=b;
			avgR+=r;
			avgG+=g;
			avgB+=b;
			}
			else{
				red.at(i).push_back(0);
				blue.at(i).push_back(0);
				green.at(i).push_back(0);
			}
		}
	}
	avgR/=sizeY*sizeX;
	avgG/=sizeY*sizeX;
	avgB/=sizeY*sizeX;
}

void imagePPM::readFileFlip(string fileName){
	cout << fileName;
	ifstream iStream(fileName);
	//Check if the file is opened
	if(!iStream.is_open()){
		cout << "Error: File Does not exist\n";
	}
	string skip;
	char s;
	int r,g,b;
	iStream >> skip >> sizeX >> sizeY >> r;
	//This portion is for skipping the header of the ppm file
	cout << skip << " "<< sizeX << " " << sizeY;
	/*This loop rights the files to the array as well as run calculations
	For the maximum minimum and average values for each color*/
	//clears vectors
	red.clear();
	green.clear();
	blue.clear();
	avgR=0;
	avgG=0;
	avgB=0;

	for(int i=0;i < sizeY; i++){
		red.push_back(vector<int>());
		green.push_back(vector<int>());
		blue.push_back(vector<int>());
		for(int j=0;j<sizeX and iStream >> r >> g >> b;j++){
			red.at(i).push_back(r);
			blue.at(i).push_back(b);
			green.at(i).push_back(g);
			if(r>maxR)
				maxR=r;
			if(g>maxG)
				maxG=g;
			if(b>maxB)
				maxB=b;
			if(r<minR)
				minR=r;
			if(g<minG)
				minG=g;
			if(b<minB)
				minB=b;
			avgR+=r;
			avgG+=g;
			avgB+=b;
		}
	}
	avgR/=sizeY*sizeX;
	avgG/=sizeY*sizeX;
	avgB/=sizeY*sizeX;
}



/*This fileWrite function writes the ppm file*/
void imagePPM::fileWrite(string fileOutName){
	ofstream out;
	out.open(fileOutName);
	out << "P3" << endl << sizeX << " " << sizeY << "\n255\n"; //This line is for header data
	for(int i=0;i<sizeY;i++)
		for(int j=0;j<sizeX;j++)
			out << red.at(i).at(j) << " " << green.at(i).at(j) << " " << blue.at(i).at(j) << " ";
}

/*This function takes a imagePPM object and converts it to a two tone image
The ints are for specifyign the color.*/
void imgBlackAndWhite(imagePPM& image){
	//This loop is for converting the image to a black and white picture
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX;j++){
			int avg=(image.red.at(i).at(j)+image.blue.at(i).at(j)+image.green.at(i).at(j))/3;
			if(!(image.green.at(i).at(j)>image.red.at(i).at(j) && image.green.at(i).at(j)>image.blue.at(i).at(j))){
				image.red[i][j]=avg;
				image.green[i][j]=avg;
				image.blue[i][j]=avg;
			}
	}
}

void imgTwoTone(imagePPM& image, int red, int blue, int green){
	//This loop is for converting the image to a black and white picture
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX;j++){
			int avg=(image.red.at(i).at(j)+image.blue.at(i).at(j)+image.green.at(i).at(j))/3;
			if(!(image.green.at(i).at(j)>image.red.at(i).at(j) && image.green.at(i).at(j)>image.blue.at(i).at(j))){
				image.red[i][j]=avg;
				image.green[i][j]=avg;
				image.blue[i][j]=avg;
			}
	}
	//This loop changes that black and white picture into a two tone image
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX;j++){
			int avg=(image.avgR+image.avgG+image.avgB)/2;
			if(image.red[i][j]+image.blue[i][j]+image.green[i][j]==0)
				continue;
			if(image.red[i][j]<avg)
				image.red[i][j]=red/2; //This divides the input color by half
			if(image.green[i][j]<avg) //For the darker tones
				image.green[i][j]=green/2;
			if(image.blue[i][j]<avg)
				image.blue[i][j]=blue/2;
			if(image.red[i][j]>=avg)
				image.red[i][j]=red;
			if(image.green[i][j]>=avg)
				image.green[i][j]=green;
			if(image.blue[i][j]>=avg)
				image.blue[i][j]=blue;
	}
}

void imgFourTone(imagePPM& image, int red, int blue, int green){
	//This loop is for converting the image to a black and white picture
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX;j++){
			image.red[i][j]=(image.red[i][j]+image.green[i][j]+image.blue[i][j])/3;
			image.green[i][j]=(image.red[i][j]+image.green[i][j]+image.blue[i][j])/3;
			image.blue[i][j]=(image.red[i][j]+image.green[i][j]+image.blue[i][j])/3;
		}
	//This loop changes that black and white picture into a two tone image
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX;j++){
			int avg=255;
			int origRed=image.red[i][j];
			int origGreen=image.green[i][j];
			int origBlue=image.blue[i][j];
			image.red[i][j]=red; //This divides the input color by half
			image.green[i][j]=green;
			image.blue[i][j]=blue;
			avg-=(( image.avgR+image.avgG+image.avgB)/3 * 0.5);
			//cerr << ":1:"<< avg;
			if(origRed<avg)
				image.red[i][j]=red*.75; //This divides the input color by half
			if(origGreen<avg) //For the darker tones
				image.green[i][j]=green*.75;
			if(origBlue<avg)
				image.blue[i][j]=blue*.75;

			avg=(image.avgR+image.avgG+image.avgB)/3;
			//cerr << ":2:"<< avg;
			if(origRed<avg)
				image.red[i][j]=red*.5; //This divides the input color by half
			if(origGreen<avg) //For the darker tones
				image.green[i][j]=green*.5;
			if(origBlue<avg)
				image.blue[i][j]=blue*.5;

			avg*=.5;
			//cerr << ":3:"<< avg;
			if(origRed<avg)
				image.red[i][j]=red*.25; //This divides the input color by half
			if(origGreen<avg) //For the darker tones
				image.green[i][j]=green*.25;
			if(origBlue<avg)
				image.blue[i][j]=blue*.25;
			


		}
	}


/*This process affects brighter colors more. A 0 0 0 black will remain constant.
Use a n*/

void replace(imagePPM& image,int redS,int redE,int greenS,int greenE,int blueS,int blueE){
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX;j++){
			if(image.red[i][j]>redS && image.red[i][j]<redE && 
			image.blue[i][j]>blueS && image.blue[i][j]<blueE &&
			image.green[i][j]>greenS && image.green[i][j]<greenE) {
				image.red[i][j]=0;
				image.blue[i][j]=0;
				image.green[i][j]=0;
			}
		}
}
/*
void upShift(imagePPM& image){
	vector<int> tempR;
	vector<int> tempB;
	vector<int> tempG;
	for(int i=1;i<image.sizeY-1;i++)
		for(int j=0;j<image.sizeX;j++)
			if(image.red[i][j]!=0 && image.blue[i][j]!=0 && image.green[i][j]!=0){
				tempR=image.red[i-1];
				tempB=image.blue[i-1];
				tempG=image.green[i-1];
	
				image.red[i-1]=image.red[i];
				image.blue[i-1]=image.blue[i];
				image.green[i-1]=image.green[i];
	
				image.red[i]=tempR;
				image.blue[i]=tempB;
				image.green[i]=tempG;
		}
	//Swap last element
	tempR=image.red.back();
	tempB=image.blue.back();
	tempG=image.green.back();

	image.red.back()=image.red.front();
	image.blue.back()=image.blue.front();
	image.green.back()=image.green.front();

	image.red.front()=tempR;
	image.blue.front()=tempB;
	image.green.front()=tempG;
}
*/


void upShift(imagePPM& image,int x){
	imagePPM image2;
	image2.blankCanvas(image.sizeY,image.sizeX);
	for(int i=0;i<x;i++){
		image2.red[image.sizeY-x+i]=image.red[i];
		image2.blue[image.sizeY-x+i]=image.blue[i];
		image2.green[image.sizeY-x+i]=image.green[i];
	}
	for(int i=x;i<image.sizeY;i++){
		image2.red[i-x]=image.red[i];
		image2.blue[i-x]=image.blue[i];
		image2.green[i-x]=image.green[i];
	}
	for(int i=0;i<image.sizeY;i++){
		image.red[i]=image2.red[i];
		image.blue[i]=image2.blue[i];
		image.green[i]=image2.green[i];
	}
}

void stratShift(imagePPM& image,int r){
	imagePPM image2;
	image2.blankCanvas(image.sizeY,image.sizeX);
	for(int j=0;j<image.sizeY;j++){
		int x=rand()%r;
		for(int i=0;i<x;i++){
			image2.red[j][image.sizeX-x+i]=image.red[j][i];
			image2.blue[j][image.sizeX-x+i]=image.blue[j][i];
			image2.green[j][image.sizeX-x+i]=image.green[j][i];
		}
		for(int i=x;i<image.sizeX;i++){
			image2.red[j][i-x]=image.red[j][i];
			image2.blue[j][i-x]=image.blue[j][i];
			image2.green[j][i-x]=image.green[j][i];
		}
		for(int i=0;i<image.sizeX;i++){
			image.red[j][i]=image2.red[j][i];
			image.blue[j][i]=image2.blue[j][i];
			image.green[j][i]=image2.green[j][i];
		}
	}
}


void sinShift(imagePPM& image,int r){
	imagePPM image2;
	image2.blankCanvas(image.sizeY,image.sizeX);
	for(int j=0;j<image.sizeY;j++){
		int x=r;
		if((tan(j/100.0)*r)<100 && (tan(j/100.0)*r)>0)
			x=tan(j/100.0)*r;
		else if((sin(j/100.0)*r)<100 && (sin(j/100.0)*r)>0)
			x=sin(j/100.0)*r;
		else
			x=0;

		for(int i=0;i<x;i++){
			image2.red[j][image.sizeX-x+i]=image.red[j][i];
			image2.blue[j][image.sizeX-x+i]=image.blue[j][i];
			image2.green[j][image.sizeX-x+i]=image.green[j][i];
		}
		for(int i=x;i<image.sizeX;i++){
			image2.red[j][i-x]=image.red[j][i];
			image2.blue[j][i-x]=image.blue[j][i];
			image2.green[j][i-x]=image.green[j][i];
		}
		for(int i=0;i<image.sizeX;i++){
			image.red[j][i]=image2.red[j][i];
			image.blue[j][i]=image2.blue[j][i];
			image.green[j][i]=image2.green[j][i];
		}
	}
}



void removesky(imagePPM& image){
	int boxX=1;
	int boxY=1;
	for(int i=0;i<image.sizeY-boxY;i+=boxX)
		for(int j=0;j<image.sizeX-boxX;j+=boxY){
			int counter=0;
			for(int y=0;y<boxY;y++){
				for(int x=0;x<boxX;x++){
					if((image.blue[i+y][j+x]>image.red[i+y][j+x]*1.2 && image.green[i+y][j+x]>image.red[i+y][j+x]*1.2)) {
						counter++;
					}
				}
			}
			if(counter>(boxX*boxY)/2){
				for(int y=0;y<boxY;y++){
					for(int x=0;x<boxX;x++){
						if((image.blue[i+y][j+x]>image.red[i+y][j+x]*1.2 && image.green[i+y][j+x]>image.red[i+y][j+x]*1.2)) {
							image.red[i+y][j+x]=0;
							image.blue[i+y][j+x]=0;
							image.green[i+y][j+x]=0;
						}
					}
				}
			}
	}
}


void removegreen(imagePPM& image){
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX;j++)
			if((image.green[i][j]>image.red[i][j]*1 && image.green[i][j]>image.blue[i][j]*1)) {
				image.red[i][j]=0;
				image.blue[i][j]=0;
				image.green[i][j]=0;
			}
}



void redgrain(imagePPM& image,int value){//int r, int g, int b, int value){
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX;j++){
			if(rand()%(value)==0){
				for(int x=j;x<(7+j) and x<image.sizeX;x++)
					for(int y=i;y<(7+i) and y<image.sizeY;y++){
						if(rand()%2==0)image.red[y][x]=255;
					}
			}
		}
}

void flipX(imagePPM& image){
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX/2;j+=2){
			int tempr=image.red[i][j];
			int tempg=image.green[i][j];
			int tempb=image.blue[i][j];
			image.red[i][j]=image.red[i][image.sizeX-j];
			image.green[i][j]=image.green[i][image.sizeX-j];
			image.blue[i][j]=image.blue[i][image.sizeX-j];
			image.red[i][image.sizeX-j]=tempr;
			image.green[i][image.sizeX-j]=tempg;
			image.blue[i][image.sizeX-j]=tempb;
		}
}

void mirrorX(imagePPM& image){
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX/2;j++){
			image.red[i][j]=image.red[i][image.sizeX-j];
			image.green[i][j]=image.green[i][image.sizeX-j];
			image.blue[i][j]=image.blue[i][image.sizeX-j];
		}
}

void redblur(imagePPM& image, int value){
	bool flag=true;
	for(int i=1;i<image.sizeY-1;i++)
		for(int j=1;j<image.sizeX-1;j++){
			if(image.red[i][j]==value){
				flag=false;	
				if(image.red[i][j+1]<value){
					image.red[i][j+1]=value-15;
				}
				if(image.red[i][j-1]<value){
					image.red[i][j-1]=value-15;
				}
                        	if(image.red[i+1][j]<value){
					image.red[i+1][j]=value-15;
				}
				if(image.red[i-1][j]<value){
					image.red[i-1][j]=value-15;
				}
				if(image.red[i+1][j+1]<value){
					image.red[i+1][j+1]=value-15;
				}
				if(image.red[i-1][j-1]<value){
					image.red[i-1][j-1]=value-15;
				}
                        	if(image.red[i+1][j-1]<value){
					image.red[i+1][j-1]=value-15;
				}
				if(image.red[i-1][j+1]<value){
					image.red[i-1][j+1]=value-15;
				}
			}
		}
		cerr << value;
		if(value<=0 or flag)
			return;
		else
			redblur(image,value-15);
}

void grain(imagePPM& image, int value){
	for(int i=0;i<image.sizeY;i++)
		for(int j=0;j<image.sizeX;j++){
			if(value>0){
			image.red[i][j]=max(min(image.red[i][j]+rand()%value,255),0);
			image.blue[i][j]=max(min(image.blue[i][j]+rand()%value,255),0);
			image.green[i][j]=max(min(image.green[i][j]+rand()%value,255),0);
			}
			if(value<0){
                        image.red[i][j]=max(min(image.red[i][j]-rand()%value,255),0);
                        image.blue[i][j]=max(min(image.blue[i][j]-rand()%value,255),0);
                        image.green[i][j]=max(min(image.green[i][j]-rand()%value,255),0);
			}
		}
}

void scangrain(imagePPM& image, int value){
	for(int i=0;i<image.sizeY;i++){
	 	int randValue=rand()%value;
		for(int j=0;j<image.sizeX;j++){
			if(value>0){
			image.red[i][j]=max(min(image.red[i][j]+randValue,255),0);
			image.blue[i][j]=max(min(image.blue[i][j]+randValue,255),0);
			image.green[i][j]=max(min(image.green[i][j]+randValue,255),0);
			}
			if(value<0){
                        image.red[i][j]=max(min(image.red[i][j]-randValue,255),0);
                        image.blue[i][j]=max(min(image.blue[i][j]-randValue,255),0);
                        image.green[i][j]=max(min(image.green[i][j]-randValue,255),0);
			}
		}
	}
}

