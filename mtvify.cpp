#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cmath>
#include "functions.cpp"
using namespace std;

string stringFormat(int number){
        string s=to_string(number);
        if(number<10)
                s="00"+s;
        else if(number<100)
                s="0"+s;

        return "image-" + s + ".png";
}

string stringFormatPPM(int number){
        string s=to_string(number);
        if(number<10)
                s="00"+s;
        else if(number<100)
                s="0"+s;

        return "image-" + s + ".ppm";
}

int main(int argc, char *argv[]){
	//Does minimal sanity check on command usage
        if (argc != 3) {
                  cerr << "Usage: mtvify framestart frameend" << endl;
                  return 1;
        }

	

	///Creates a audiomap from an image
	imagePPM audio;	
	audio.readFile("./audio.ppm");
	vector<int> audiomap;
	for(int i=stoi(argv[1]);i<stoi(argv[2]);i++){
		for(int j=0;j<audio.sizeY;j++){
			audiomap.push_back(2);
			if(audio.blue[j][i]>20 && audiomap[i]<=252){
				audiomap[i]++;
				cout << '*';
			}
		}
		cout << '*' << endl;
	}

	imagePPM image;
	imagePPM laptop;
	//Main loop for editing images
	int counter=stoi(argv[1]);
	int flip=0;
	int flipTop=0;
	//Intro
	int red=0;
	int green=0;
	int blue=0;
	//This is for the color distortion of each image
	int distortionR=0;
	int distortionB=0;
	int distortionG=0;
	int counterImage=1;
	int mainCounter=1;
	imagePPM scanline;
	scanline.blankCanvas(768,1366);
	//This is the main loop argv 1 is for start frame argv 2 is for end frame
	//
	//Although this wasn't implented for this render
	for(int i=1;i<stoi(argv[2]);i++){
		cerr << i << endl;
		//The flip variable goes through 0 to 3 for each image file folder
		string path = "./photo"+to_string(flip)+"/"+to_string(counterImage)+".ppm";    //This sets a path variable for the image to load
		image.readFile(path); 

		laptop.readFile("./main/"+to_string(mainCounter)+".ppm"); //It also can be done directly with read file
		/*This formula uses the previous audio volume measure to detect the next one*/
		if(i>2 && (audiomap[i-1]-audiomap[i-2])>audiomap[i-1]*0.30){
			flip+=rand()%2;
			flipTop++;
			if(flip>=4){
				flip=0;
			}
			int color=rand()%220;
			int red=color+rand()%34;
			int blue=color+rand()%34;
			int green=color+rand()%34;
			distortionR=rand()%49;
			distortionB=rand()%49;
			distortionG=rand()%49;
			/*This code is for applying the scan lines*/
			for(int y=0;y<audiomap[i-1]/30;y+=rand()%4+1){
				for(int j=0;j<scanline.sizeX;j++){
					scanline.red[y][j]=red;
					scanline.blue[y][j]=blue;
					scanline.green[y][j]=green;
				}
				/*This code was orginally meant for being outside the volume portions
				 * it was mistankenly put in the section for changing volume
				 * but I decided I liked the affect and didn't want to rerender 
				 * due to 12 hour render time*/
				for(int j=0;j<scanline.sizeX;j++){
						scanline.red[scanline.sizeY-1][j]=0;
						scanline.blue[scanline.sizeY-1][j]=0;
						scanline.green[scanline.sizeY-1][j]=0;
				}
				for(int j=0;j<scanline.sizeX;j++){
						scanline.red[scanline.sizeY-2][j]=0;
						scanline.blue[scanline.sizeY-2][j]=0;
						scanline.green[scanline.sizeY-2][j]=0;
				}
				red+=(rand()%25)-10;//Creates greyish colored scan lines
				blue+=(rand()%25)-10;
				green+=(rand()%25)-10;
				if(red>255 || blue>255 || green>255){
					red=0;
					blue=0;
					green=0;
				}
			}
		}
		//Runs the upShift function it moves the scanlines up.	
		upShift(scanline,2);

		//Creates the four tone colored image using a modified version of the poster code.
		imgFourTone(image,70+audiomap[i-1]/2+distortionR,70+audiomap[i-1]/2+distortionB,70+audiomap[i-1]/2+distortionG);

                cerr << "editing image " << i << "/" << argv[2] << "  "<<  endl;
		scangrain(image,(audiomap[i-1]/4)+1);
		
		//Displays the laptop image
		if(flip%2==0){
			sinShift(laptop,audiomap[i-1]+1);
			image+=laptop;
		}
		else //Distorts the background image.
			stratShift(image,audiomap[i-1]/10+1);
		image+=scanline;
		image.fileWrite("./render/"+to_string(i)+".ppm");//Writes the image
		counterImage++;
		if(counterImage>403)//Resets the background image counter
			counterImage=1;
		mainCounter++; //This is the counter for the laptop. The footage is stored in the main folder
		if(mainCounter>2000)
			mainCounter=1;
	}
	
	return 0;
}
