#ifndef functions_h
#define functions_h
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
class imagePPM {
public:
	int maxR;
	int maxG;
	int maxB;
	int minR;
	int minG;
	int minB;
	int sizeY;
	int sizeX;
	int avgR;
	int avgG;
	int avgB;
	imagePPM();
	std::vector<std::vector<int>> red;
	std::vector<std::vector<int>> blue;
	std::vector<std::vector<int>> green;
	void blankCanvas(int,int);
	void checkers(int,int);
	void readFile(std::string);
	void readFileFlip(std::string);
	void fileWrite(std::string fileOutName);
	imagePPM& operator+=(imagePPM const&);
};

imagePPM::imagePPM() {
	avgR = 0;
	avgG = 0;
	avgB = 0;
}

imagePPM& imagePPM::operator+=(const imagePPM& img){
///	cout << "ok";
//	cout << sizeY << sizeX << img.sizeY << img.sizeX;
	for(int i=0;i<sizeY;i++)
		for(int j=0;j<sizeX;j++)
			if(img.red.at(i).at(j)+img.blue.at(i).at(j)+img.green.at(i).at(j)!=0){
				this->red[i][j]=img.red[i][j];
				this->blue[i][j]=img.blue[i][j];
				this->green[i][j]=img.green[i][j];
			}
	return *this;
}
void upShift(imagePPM&,int);
void stratShift(imagePPM&,int);
void sinShift(imagePPM&,int);
//void checkers(imagePPM&,int,int);
void imgBlackAndWhite(imagePPM&);
void imgTwoTone(imagePPM&,int,int,int);
void imgFourTone(imagePPM&,int,int,int);
void replace(imagePPM&,int,int,int,int,int,int);
void removegreen(imagePPM& image);
void grain(imagePPM&,int);
void scangrain(imagePPM&,int);
void redgrain(imagePPM&,int);
void redblur(imagePPM&,int);
void flipX(imagePPM&);
void mirrorX(imagePPM&);
void colorShift(imagePPM&);
#endif
